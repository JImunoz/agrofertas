{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{* Structured Data Json - LD Microdata for Prestashop 1.6.X
*
* Add this code in your smarty global.tpl/header.tpl file to show Organization, WebPage, Website and Product Microdata in ld+json format.
* Requires Prestashop 'productcomments' module for review stars ratings.
* by Ruben Divall @rubendivall http://www.rubendivall.com
*}

<script type="application/ld+json">
{
    "@context" : "http://schema.org",
    "@type" : "Organization",
    "name" : "{$shop.name|escape:'html':'UTF-8'}",
    "url" : "{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$urls.base_url}{/if}",
    "logo" : {
        "@type":"ImageObject",
        "url":"{$shop.logo}"
    }
}
</script>
<script type="application/ld+json">
{
    "@context":"http://schema.org",
    "@type":"WebPage",
    "isPartOf": {
        "@type":"WebSite",
        "url":  "{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$urls.base_url}{/if}",
        "name": "{$shop.name|escape:'html':'UTF-8'}"
    },
    "name": "{$page.meta.title}",
    "url":  "{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{trim($smarty.server.REQUEST_URI,'/')}{else}{$urls.base_url}{trim($smarty.server.REQUEST_URI,'/')}{/if}"
}
</script>
{if $page.page_name =='index'}
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type":  "WebSite",
    "url":  "{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$urls.base_url}{/if}",
    "image" : {
    "@type":  "ImageObject",
    "url":  "{$shop.logo}"
    },
    "potentialAction": {
        "@type": "SearchAction",
        "target": "{$link->getPageLink('search', null, null, null, false, null,  true)|escape:'html':'UTF-8'}?search_query={literal}{search_term}{/literal}",
        "query-input": "required name=search_term"
    }
}
</script>
{/if}

{*{if $page.page_name == 'product'}
<script type="application/ld+json">
    {  
    "@context": "http://schema.org/",
    "@type": "Product",
    "name": "{$product->name}",
    "image": "{$product.cover.bySize.home_default.url|escape:'html':'UTF-8'}",
    "description": "{$product.description_short|strip_tags|escape:'html':'UTF-8'}",
    {if $product.reference}
    "mpn": "{$product.reference|escape:'html':'UTF-8'}",
    {/if}
    {if $product.ean13}
    "gtin13": "{$product.ean13|escape:'html':'UTF-8'}",
    {/if}
    {if $product_manufacturer->name}
    "brand": {
        "@type": "Thing",
        "name": "{$product_manufacturer->name|escape:'html':'UTF-8'}"
    },
    {/if}
    {if $nbComments && $ratings.avg}
    "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "{$ratings.avg|round:1|escape:'html':'UTF-8'}",
        "reviewCount": "{$nbComments|escape:'html':'UTF-8'}"
    },
    {/if}
    "offers": {
        "@type": "Offer",
        "priceCurrency": "{$currency->iso_code}",
        "price": "{$product->getPrice(true, $smarty.const.NULL, 2)|round:'2'}",
   
        {if $product->condition == 'new'}"itemCondition": "http://schema.org/NewCondition",{/if}
        {if $product->condition == 'used'}"itemCondition": "http://schema.org/UsedCondition",{/if}
        {if $product->condition == 'refurbished'}"itemCondition": "http://schema.org/RefurbishedCondition",{/if}
        {if $product->quantity > 0}"availability": "http://schema.org/InStock",{/if}
        "seller": {
            "@type": "Organization",
            "name": "{$shop_name|escape:'html':'UTF-8'}"
            }
    }
}
</script>
{/if}*}
{** End of Structured Data Json - LD Microdata for Prestashop 1.6.X **}

{block name='header_banner'}
  <div class="header-banner">
    {hook h='displayBanner'}
  </div>
{/block}

{block name='header_nav'}
  <nav class="header-nav">
    <div class="container">
      <div class="row">
        <div class="hidden-sm-down">
          <div class="col-md-5 col-xs-12">
            {hook h='displayNav1'}
          </div>
          <div class="col-md-7 right-nav">
              {hook h='displayNav2'}
          </div>
        </div>
        <div class="hidden-md-up text-sm-center mobile">
          <div class="float-xs-left" id="menu-icon">
            <i class="material-icons d-inline">&#xE5D2;</i>
          </div>
          <div class="float-xs-right" id="_mobile_cart"></div>
          <div class="float-xs-right" id="_mobile_user_info"></div>
          <div class="top-logo" id="_mobile_logo"></div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </nav>
{/block}

{block name='header_top'}
  <div class="header-top">
    <div class="container">
       <div class="row">
        <div class="col-md-2 hidden-sm-down" id="_desktop_logo">
            {if $page.page_name == 'index'}
              <h1>
                <a href="{$urls.base_url}">
                  <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
                </a>
              </h1>
            {else}
                <a href="{$urls.base_url}">
                  <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
                </a>
            {/if}
        </div>
        <div class="col-md-10 col-sm-12 position-static">
          {hook h='displayTop'}
          <div class="clearfix"></div>
        </div>
      </div>
      <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
        <div class="js-top-menu-bottom">
          <div id="_mobile_currency_selector"></div>
          <div id="_mobile_language_selector"></div>
          <div id="_mobile_contact_link"></div>
        </div>
      </div>
    </div>
  </div>
  {hook h='displayNavFullWidth'}
{/block}
